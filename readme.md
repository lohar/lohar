<!--
SPDX-License-Identifier: "CC-BY-SA-4.0"
Copyright (C) 2021-2021, Jayesh Badwaik <j.badwaik@fz-juelich.de>
-->

# sutar: a collection of tools for software development

## Encryption

### Usage

```
$ sutar.encrypt <plain-file> <passphrase> <salt> <encrypted-file>
$ sutar.decrypt <encrypted-file> <passphrase> <salt> <plain-file>
```

### Requirements
1. Password: Arbitrary String  (Max Length: 2^1024-1)
2. Salt: 16 byte Hex String

### Encryption Algorithm

```
Algorithm: AES
Key Size: 256
Block Cipher Mode: CBC
Key Derivation: PBKDF2
Key Derivation Iterations: 20000
Hashing Algorithm: SHA512
```

## Setting up SSH on a Server

### Usage
```
# sutar.setup.ssh \
  <target-address> \
  <target-port> \
  <target-username> \
  <target-hostname> \
  <identity-file> \
  <known-hosts-file> \
  <ssh-config-file> \
  <host-username>
```

## VPS Setup


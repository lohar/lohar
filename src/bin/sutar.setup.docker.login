#!/bin/bash
#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

set -euo pipefail
if [ "$#" -ne 3 ]; then
  echo "Script provided with $# arguments."
  echo "Script requires :"
  echo "1. Docker Registry"
  echo "2. Username File"
  echo "3. Password File"
  exit 1
fi

DOCKER_REGISTRY=$1
USERNAME_FILE=$2
PASSWORD_FILE=$3

USERNAME=$(cat $USERNAME_FILE)

cat $PASSWORD_FILE | docker login -u $USERNAME --password-stdin $DOCKER_REGISTRY


#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_encrypt_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar encrypt <args>" "Encrypt "

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-i|--input <input-file>" "Input File"
  printf "  %-40s %-30s\n"  "-o|--output <output-file>" "Output File"
  printf "  %-40s %-30s\n"  "-p|--passphrase <passphrase>" "Passphrase"
  printf "  %-40s %-30s\n"  "-s|--salt <salt>" "Salt"
}

sutar_encrypt() {
  INPUT_SPECIFIED="false"
  OUTPUT_SPECIFIED="false"
  PASSPHRASE_SPECIFIED="false"
  SALT_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_encrypt_print_help
        exit 0
        ;;
      -i|--input)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        INPUT_FILE=$1
        INPUT_SPECIFIED="true"
        shift
        ;;
      -o|--output)
        shift
        if [[ $# -eq 0 ]]; then
          echo "Expected a value"
          break
        fi
        OUTPUT_FILE=$1
        OUTPUT_SPECIFIED="true"
        shift
        ;;
      -p|--passphrase)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        PASSPHRASE=$1
        PASSPHRASE_SPECIFIED="true"
        shift
        ;;
      -s|--salt)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        SALT=$1
        SALT_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_encrypt_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $INPUT_SPECIFIED == "false" ]]; then
    echo "Missing Input File."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $OUTPUT_SPECIFIED == "false" ]]; then
    echo "Missing Output File."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PASSPHRASE_SPECIFIED == "false" ]]; then
    echo "Missing Passphrase."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $SALT_SPECIFIED == "false" ]]; then
    echo "Missing Salt."
    PRINT_HELP_AND_EXIT="true"
  fi

 echo ""

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_encrypt_print_help
    exit 1
  fi

  openssl enc \
    -aes-256-cbc -md sha512 -pbkdf2 -iter 20000 \
    -in $INPUT_FILE \
    -out $OUTPUT_FILE \
    -k $PASSPHRASE \
    -S $SALT
  }


#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_genpass_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar genpass <args>" "Generate Password"

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-l|--length <length>" "Length of the Password"
}

sutar_genpass() {
  LENGTH_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_genpass_print_help
        exit 0
        ;;
      -l|--length)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        LENGTH=$1
        LENGTH_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_genpass_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $LENGTH_SPECIFIED == "false" ]]; then
    echo "Missing Length."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_genpass_print_help
    exit 1
  fi

  RAW_LENGTH=$((LENGTH+128))
  openssl rand -base64 $RAW_LENGTH | tr -d '\n '| head -c $LENGTH
  printf "\n"
}


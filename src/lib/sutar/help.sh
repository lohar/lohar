#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------

sutar_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar <op> --help" "Print help for <op>"
  printf "  %-30s %-30s\n"  "sutar <op> <op-args>" "Carry out operation <op> with <op-args> arguments."

  echo ""
  echo "sutar is a development helper"
  echo ""


  echo "Operations:"
  printf "  %-20s %-30s\n"  "decrypt" "Decrypt a File"
  printf "  %-20s %-30s\n"  "encrypt" "Encrypt a File"
  printf "  %-20s %-30s\n"  "genpass" "Generate Password"
  printf "  %-20s %-30s\n"  "gensalt" "Generate Salt"
  printf "  %-20s %-30s\n"  "ssh" "SSH Operations"
  printf "  %-20s %-30s\n"  "setup.login.docker" "Setup Docker Login"
  printf "  %-20s %-30s\n"  "setup.login.ssh" "Setup SSH Login"
}

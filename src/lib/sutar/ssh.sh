#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_ssh_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar ssh <op> <args>" "Generate Password"

  echo ""
  echo "Operations:"
  printf "  %-40s %-30s\n"  "scan" "Scan"
}

sutar_ssh_scan_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar ssh scan <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-t|--target target" "Target to Scan"
  echo "Optional Arguments:"
  printf "  %-40s %-30s\n"  "-p|--port port" "Port (Default: 22)"
}

sutar_ssh_scan() {
  TARGET_SPECIFIED="false"
  PORT_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_ssh_scan_print_help
        exit 0
        ;;
      -t|--target)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        TARGET=$1
        TARGET_SPECIFIED="true"
        shift
        ;;
      -p|--port)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        PORT=$1
        PORT_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_ssh_scan_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $TARGET_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PORT_SPECIFIED == "false" ]]; then
    PORT="22"
    PORT_SPECIFIED="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_ssh_scan_print_help
    exit 1
  fi

  ssh-keyscan -p $PORT $TARGET
}


sutar_ssh_setup_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar ssh setup <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-t|--target target" "Target to setup"
  echo "Optional Arguments:"
  printf "  %-40s %-30s\n"  "-i|--id id" "Identity File (Default: $HOME/.ssh/id_rsa.pub)"
  printf "  %-40s %-30s\n"  "-k|--knownhosts known_hosts_file" "Known Hosts File (Default: $HOME/.ssh/known_hosts)"
  printf "  %-40s %-30s\n"  "-u|--user username" "Username (Default: current user)"
  printf "  %-40s %-30s\n"  "-a|--address address" "Target (Default: <target>)"
  printf "  %-40s %-30s\n"  "-p|--port port" "Port (Default: 22)"
}

sutar_ssh_setup() {
  TARGET_SPECIFIED="false"
  ID_FILE_SPECIFIED="false"
  KNOWN_HOSTS_SPECIFIED="false"
  USER_SPECIFIED="false"
  ADDRESS_SPECIFIED="false"
  PORT_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_ssh_setup_print_help
        exit 0
        ;;
      -t|--target)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        TARGET=$1
        TARGET_SPECIFIED="true"
        shift
        ;;
      -i|--id)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        ID_FILE=$1
        ID_FILE_SPECIFIED="true"
        shift
        ;;
      -k|--knownhosts)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        KNOWN_HOSTS=$1
        KNOWN_HOSTS_SPECIFIED="true"
        shift
        ;;
      -u|--user)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        USER=$1
        USER_SPECIFIED="true"
        shift
        ;;
      -a|--address)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        ADDRESS=$1
        ADDRESS_SPECIFIED="true"
        shift
        ;;
      -p|--port)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        PORT=$1
        PORT_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_ssh_setup_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $TARGET_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $ADDRESS_SPECIFIED == "false" ]]; then
    if [[ $TARGET_SPECIFIED != "false" ]]; then
      ADDRESS=$TARGET
      ADDRESS_SPECIFIED="true"
    else
      echo "Missing Target."
      PRINT_HELP_AND_EXIT="true"
    fi
  fi



  if [[ $KNOWN_HOSTS_SPECIFIED == "false" ]]; then
    KNOWN_HOSTS="~/.ssh/known_hosts"
    KNOWN_HOSTS_SPECIFIED="true"
  fi

  if [[ $USER_SPECIFIED == "false" ]]; then
    USER="$(whoami)"
    USER_SPECIFIED="true"
  fi

  if [[ $ID_FILE_SPECIFIED == "false" ]]; then
    ID_FILE="~/.ssh/id_rsa.pub"
    ID_FILE_SPECIFIED="true"
  fi

  if [[ $PORT_SPECIFIED == "false" ]]; then
    PORT="22"
    PORT_SPECIFIED="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_ssh_setup_print_help
    exit 1
  fi

  printf "Host $TARGET"
  printf "\n\tHostName $ADDRESS"
  printf "\n\tPort $PORT"
  printf "\n\tUser $USER"
  printf "\n\tIdentityFile $ID_FILE"
  printf "\n\tUserKnownHostsFile $KNOWN_HOSTS\n"
}






sutar_ssh() {
  if [[ $# -eq 0 ]]; then
    sutar_ssh_print_help
    exit 1
  fi


  SUBCOMMAND=$1
  shift
  case $SUBCOMMAND in
    -h|--help)
      sutar_ssh_print_help
      exit 0
      ;;
    scan)
      sutar_ssh_scan $@
      exit 0
      ;;
    setup)
      sutar_ssh_setup $@
      exit 0
      ;;
    *)
      echo "Unrecognized Operation $SUBCOMMAND"
      sutar_ssh_print_help
      exit 1
  esac
}


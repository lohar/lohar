sutar_vps_nginx_install() {
  if [ "$#" -ne 3 ]; then
    echo "Script requires:"
    echo "1. Distribution"
    echo "2. Release"
    echo "3. Target Hostname"
    exit 1
  fi


  DISTRIBUTION=$1
  RELEASE=$2
  TARGET_HOSTNAME=$3

  ssh -t $TARGET_HOSTNAME "sudo apt-get install -y nginx python3-certbot-nginx certbot"
  ssh -t $TARGET_HOSTNAME "sudo ufw allow \"Nginx Full\""
  ssh -t $TARGET_HOSTNAME "sudo unlink /etc/nginx/sites-enabled/default"
}

sutar_vps_nginx_website() {
  if [ "$#" -ne 4 ]; then
    echo "Script requires:"
    echo "1. Target Hostname"
    echo "2. Website Name"
    echo "3. Username"
    echo "4. Email"
    exit 1
  fi

  TARGET_HOSTNAME=$1
  WEBSITE=$2
  USERNAME=$3
  EMAIL=$4

  ssh -t $TARGET_HOSTNAME "sudo mkdir -p /srv/https/$WEBSITE"
  ssh -t $TARGET_HOSTNAME "sudo chown -vR $USERNAME:$USERNAME /srv/https/$WEBSITE"

  TMPDIR=$(mktemp -d)

  echo $WEBSITE >> $TMPDIR/index.html

  cat > $TMPDIR/$WEBSITE <<- EOM
server {
  listen 80;
  listen [::]:80;

  root /srv/https/$WEBSITE;
  index index.html;

  server_name $WEBSITE;

  location / {
    try_files \$uri \$uri/ =404;
  }
}
EOM

  rsync -aAX $TMPDIR/index.html $TARGET_HOSTNAME:/srv/https/$WEBSITE/index.html
  rsync -aAX $TMPDIR/$WEBSITE $TARGET_HOSTNAME:/tmp/$WEBSITE

  ssh -t $TARGET_HOSTNAME "sudo cp /tmp/$WEBSITE /etc/nginx/sites-available/$WEBSITE"

  ssh -t $TARGET_HOSTNAME "sudo ln -s /etc/nginx/sites-available/$WEBSITE /etc/nginx/sites-enabled/$WEBSITE"

  ssh -t $TARGET_HOSTNAME "sudo certbot --nginx -n -d $WEBSITE --agree-tos --redirect -m $EMAIL --webroot -w /srv/https/$WEBSITE"
}
